<html>

    <head>
        <title>First Web Application</title>
        <link href="webjars/bootstrap/4.1.3/css/bootstrap.min.css"
              rel="stylesheet">
    </head>

    <body>
        Welcome ${name}!! <a href="/list-todos">Click here</a> to manage your todo's.

        <div class="container">
            <table class="table table-striped">
                <caption>Your todos are</caption>
                <thead>
                <tr>
                    <th>Description</th>
                    <th>Target Date</th>
                    <th>Is it Done?</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Todo 1</td>
                    <td>10/12/2017</td>
                    <td>No</td>
                    <td><a class="btn btn-warning" href="/edit-todo">Edit Todo</a></td>
                    <td><a class="btn btn-warning" href="/delete-todo">Delete Todo</a></td>
                </tr>
                </tbody>
            </table>
            <div>
                <a class="btn btn-default" href="/add-todo">Add a Todo</a>
            </div>

            <script src="webjars/jquery/3.3.1/jquery.min.js"></script>
            <script src="webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        </div>

    </body>

</html>