package com.in28minutes.springboot.studentservices.web.repository;

import com.in28minutes.springboot.studentservices.web.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
}
